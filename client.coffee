# Example usage
# POST with custom headers & data: client 'example.com/test', {cookie: 'test=true'}, 'test', (res) -> console.log res.body
# GET  with custom headers:        client 'example.com/test', {cookie: 'test=true'}, (res) -> console.log res.body
# POST with data:                  client 'example.com/test', 'test', (res) -> console.log res.body
# GET:                             client 'example.com/test', (res) -> console.log res.body
client = (address, arg1, arg2, arg3) ->
	options =
		host: address.replace /[\/:].*/, ''
		path: address.replace /[^\/]*/,  ''
		port: Number(address.match(/:([0-9]*)\//)?[1]) or 80
	switch
		when arg3?
			options.headers = arg1
			data     = arg2
			callback = arg3
		when arg2?
			if typeof arg1 is 'string'
				options.headers = {}
				data = arg1
			else
				options.headers = arg1
			callback = arg2
		else
			callback = arg1
	if data?
		unless options.headers['Content-Type']?   then options.headers['Content-Type']   = 'application/x-www-form-urlencoded'
		unless options.headers['Content-Length']? then options.headers['Content-Length'] = Buffer.byteLength data
		options.method = 'POST'
	else options.method = 'GET'
	console.dir
		options: options
		arg1: arg1
		arg2: arg2
		arg3: arg3
	req = http.request options, (res) ->
		res.setEncoding 'utf8'
		reply = ''
		res.on 'data', (chunk) ->
			reply += chunk
		res.on 'end', ->
			res.body = reply
			callback res
	req.write data if data?
	req.end()

module.exports = exports = client